
$(document).ready(function() {
    $('.menu-trigger').click(function() {
        $('.navigation ul').slideToggle(500);
    });//end slide toggle

    $(window).resize(function() {
        if (  $(window).width() > 1200 ) {
            $('.navigation ul').removeAttr('style');
        }
    });//end resize
});//end ready

var screen = screen.width;

if (screen <= 992){
    document.addEventListener("DOMContentLoaded", function () {
        jQuery('.main-block .slider').slick({
            dots: false,
            prevArrow: false,
            nextArrow: false,
            infinite: true,
            speed: 400,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
            ]
        });
    });
}

